 <?php  
    $cover_title            = get_post_meta( 6, 'cover_title', true );
    $cover_button_text      = get_post_meta( 6, 'cover_button_text', true );
    ?>

	<section id="cover">
        <div id="cover-caption">
            <div class="container">
                <div class="col-md-10 col-sm-9 col-xs-12 col-sm-offset-1">
                    <div class="row">
                        <h1 class="display-3"><?php echo $cover_title; ?></h1>
                    </div>
                    <div class="row b-left">        
                        <button class="btn btn-s btn-lg"><?php echo $cover_button_text; ?></button>
                    </div>
                    <div class="">
                         <a href="#nav-main" class="awesome btn btn-lg" role="button"><span class="fa fa-angle-down" aria-hidden="true"></span></a>
                    </div>
                </div>
            </div>
        </div>
    </section>