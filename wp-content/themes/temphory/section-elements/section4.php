<?php 
    $post_data = get_post( 6 );
    global $post;
    $post = $post_data;
    setup_postdata($post);
    $income_section4_title                          = get_field('income_section4_title');
    $income_section4_title_quote                    = get_field('income_section4_title_quote');
    $income_section4_button_name                    = get_field('income_section4_button_name');
    $income_section4_text_title                     = get_field('income_section4_text_title');
    $income_section4_textarea                       = get_field('income_section4_textarea');
    $income_section4_buttonleft_under_textarea      = get_field('income_section4_buttonleft_under_textarea');
    $income_section4_buttonright_under_textarea     = get_field('income_section4_buttonright_under_textarea');
    $under_section4_images                          = get_field('under_section4_images');

?>

<section id="cover4">
		<div class="section-content3 col-md-12">
			<div class="k45up-orange"></div>
            <div class="container">
                <div class="row">
				    <div class="block-left col-md-offset-right-1 col-lg-4 col-md-10 col-sm-10">
				        <h2 class="up-title"><?php echo $income_section4_title; ?></h2>
				        <h4><?php echo $income_section4_title_quote; ?></h4>
				        <button class="btn btn-s2 btn-lg"><?php echo $income_section4_button_name; ?></button>
				    </div>
                    <div class="block-left col-lg-1"></div>
				    <div class="block-right col-lg-7 col-md-10 col-sm-10">
                        <h3 class="up-title"><span class="black-text t-overflow"><?php echo $income_section4_text_title; ?></span></h3>
                        <p class="lead"><?php echo $income_section4_textarea; ?></p>
                        <div class="formos form-group row pull-right">
                            <button class="btn btn-ac btn-lg"><?php echo $income_section4_buttonleft_under_textarea; ?></button>
                            <button class="btn btn-ac btn-lg"><?php echo $income_section4_buttonright_under_textarea; ?></button>
                        </div>
				    </div>
                </div>
                <div class="k45-4"></div>
            </div>
        </div>
</section>
	
<section id="cover4-5" style="background:url('<?php echo $under_section4_images['url']; ?>') center center;background-size:cover"></section>