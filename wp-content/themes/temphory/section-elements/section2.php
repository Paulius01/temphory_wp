<?php 
    $post_data = get_post( 6 );
    global $post;
    $post = $post_data;
    setup_postdata($post);
    $income_section2_title          = get_field('income_section2_title');
    $income_section2_title_quote    = get_field('income_section2_title_quote');
    $income_section2_button_name    = get_field('income_section2_button_name');
    $income_section2_text_title     = get_field('income_section2_text_title');
    $income_section2_textarea       = get_field('income_section2_textarea');
    $under_section2_images          = get_field('under_section2_images');

?>


<section id="cover2">
		<div class="section-content col-md-12">
			<div class="container">
                <div class="row">
				    <div class="block-left col-lg-4 col-md-10 col-sm-10">
				        <h2 class="up-title"><?php echo $income_section2_title; ?></h2>
                        <h4><?php echo $income_section2_title_quote; ?></h4>
                        <button class="btn btn-s2 btn-lg"><?php echo $income_section2_button_name; ?></button>
				    </div>
				    <div class="block-left col-lg-1"></div>
				    <div class="block-right col-lg-7 col-md-10 col-sm-10">
				        <h3 class="title up-title"><?php echo $income_section2_text_title; ?></h3>
                        <p class="lead"><?php echo $income_section2_textarea; ?></p>
				    </div>
				    <div class="k45"></div>
				</div>
			</div>
		</div>
	</section>
	
   <section id="cover2-3" style="background:url('<?php echo $under_section2_images['url']; ?>') center center;background-size:cover"></section>