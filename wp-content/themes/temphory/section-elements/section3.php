<?php 
    $post_data = get_post( 6 );
    global $post;
    $post = $post_data;
    setup_postdata($post);
    $income_section3_title          = get_field('income_section3_title');
    $income_section3_title_quote    = get_field('income_section3_title_quote');
    $income_section3_button_name    = get_field('income_section3_button_name');
    $income_section3_text_title     = get_field('income_section3_text_title');
    $income_section3_textarea       = get_field('income_section3_textarea');
    $under_section3_images          = get_field('under_section3_images');

?>

<section id="cover3">
    <div class="section-content2 col-md-12">
        <div class="k45up"></div>
        <div class="container">
            <div class="row">
                <div class="block-left col-md-offset-right-1  col-lg-4 col-md-10 col-sm-10">
                    <h2 class="up-title"><?php echo $income_section3_title; ?></h2>
                    <h4><?php echo $income_section3_title_quote; ?></h4>
                    <button class="btn btn-s btn-lg"><?php echo $income_section3_button_name; ?></button>
                </div>
                <div class="block-left col-lg-1"></div>
                <div class="block-right col-lg-7 col-md-10 col-sm-10">
                    <h3 class="title2 up-title"><?php echo $income_section3_text_title; ?></h3>
                    <p class="lead"><?php echo $income_section3_textarea; ?></p>
                </div>
            <div class="k45down"></div>
            </div>
        </div>
    </div>
</section>
            
<section id="cover3-4" style="background:url('<?php echo $under_section3_images['url']; ?>') center center;background-size:cover"></section>