<?php 
    $post_data = get_post( 6 );
    global $post;
    $post = $post_data;
    setup_postdata($post);
    $income_section5_title                          = get_field('income_section5_title');
    $income_section5_title_quote                    = get_field('income_section5_title_quote');
    $income_section5_button_name                    = get_field('income_section5_button_name');
    $income_section5_textarea                       = get_field('income_section5_textarea');
    $under_section5_images                          = get_field('under_section5_images');
?>


<section id="cover5">
    <div class="section-content4 col-md-12">
        <div class="k45up"></div>
            <div class="container">
                <div class="row">
				    <div class="block-left col-md-offset-right-1 col-lg-4 col-md-10 col-sm-10">
				        <h2 class="up-title"><?php echo $income_section5_title; ?></h2>
				        <h4><?php echo $income_section5_title_quote; ?></h4>
				        <button id="click" class="btn btn-s btn-lg"><?php echo $income_section5_button_name; ?></button>
				    </div>
				    <div class="block-right col-md-offset-1 col-lg-7 col-md-7 col-md-10 col-sm-12">
				   
                        <p class="lead lead-p"><?php echo $income_section5_textarea; ?></p>
                        
                        
                        <?php echo do_shortcode( '[contact-form-7 id="41" title="Primary Contact Form"]' ); ?>
                        

                    </div>
                </div>
            </div>
        </div>
        <div class="k45down"></div>
</section>
        
<section id="cover5-6" style="background:url('<?php echo $under_section5_images['url']; ?>') center center;background-size:cover"></section>