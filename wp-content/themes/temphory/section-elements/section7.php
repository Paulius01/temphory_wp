<section id="cover7">
    <div class="section-content6 col-lg-12">
        <div class="k45up"></div>
        <div class="container">
            <div class="row">
                <div class="block-left col-lg-6 col-md-6 col-sm-10">
                    <h2><span class="organge-text">Temphory</span></h2>
                    <p class="lead">Ben jij een vakman of vakvrouw en wil je investeren in je ontwikkeling? 
				    Of ben je op zoek naar vakmensen? Neem dan contact met ons op via 
				    <span class="organge-text">0341 424 080</span> of vul het contactformulier in.
                    </p>
				</div>
				<div class="block-right col-lg-3 col-md-3 col-sm-5">
				    <ul class="lead">
				        <li>Middelerf 14b</li>
				        <li>3851 SP Ermelo</li>
				        <li>Tel.: 0341 424 080</li>
				        <li>Privacy statement</li>
				    </ul>
				</div>
				<div class="block-right col-lg-3 col-md-3 col-sm-5">
				    <ul class="lead">
				        <li>Postadres:</li>
				        <li>Postbus 473</li>
				        <li>3840 AL Harderwijk</li>
				        <li>E-mail: <span class="organge-text">contact@temphory.com</span></li>
				    </ul>
                </div>
                
            </div>
            <div class="col-sm-3">
                    <h4><?php bloginfo('name'); ?> &copy; <?php echo date('Y') ?></h4>
                </div>
        </div>
    </div>
</section>