<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Temphory
 */

?>

<?php 
    $post_data = get_post( 51 );
    global $post;
    $post = $post_data;
    setup_postdata($post);
    $income_title                   = get_field('income_title');
    $income_text                    = get_field('income_text');
    
?>

<?php wp_footer(); ?>

<section id="cover7">
    <div class="section-content6">
        <div class="k45up"></div>
        <div class="container">
            <div class="row">
                <div class="block-left col-lg-6 col-md-6 col-sm-10">
                    <h2 class="up-title"><span class="organge-text"><?php echo $income_title; ?></span></h2>
                    <p class="lead"><?php echo $income_text; ?>
                    </p>
				</div>
				<div class="block-right col-lg-3 col-md-3 col-sm-5">
				    <ul class="lead">
				        <li>Middelerf 14b</li>
				        <li>3851 SP Ermelo</li>
				        <li>Tel.: 0341 424 080</li>
				        <li>Privacy statement</li>
				    </ul>
				</div>
				<div class="block-right col-lg-3 col-md-3 col-sm-5">
				    <ul class="lead">
                        <li>Postadres:</li>
                        <li>Postbus 473</li>
						<li>3840 AL Harderwijk</li>
						<li>E-mail: <span class="organge-text">contact@temphory.com</span></li>
				    </ul>                        
                </div>
                
            </div>
            <div class="col-sm-3">
                    <h4><?php bloginfo('name'); ?> &copy; <?php echo date('Y') ?></h4>
                </div>
        </div>
    </div>
</section>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>



<script src="<?php bloginfo('template_directory'); ?>/assets/js/bootstrap.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/assets/js/index.js"></script>


</body>
</html>
