<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Temphory
 */

get_header(); ?>

    <section id="cover">
        <div id="cover-caption">
            <div class="container">
                <div class="col-md-10 col-sm-9 col-xs-12 col-sm-offset-1">
                    <div class="row">
                        <h1 class="display-3">404 error<br>page not found</h1>
                    </div>
                    <div class="row b-left">        
                        <button class="btn btn-s btn-lg"><a href="<?php echo esc_url( home_url( '/' ) ); ?>">Home Page</a></button>
                    </div>
                </div>
            </div>
        </div>    
    </section>

	

<?php
get_footer();
