<?php
/* Template Name: Contact Page */

get_header(); ?>


<?php 
    $post_data = get_post( 72 );
    global $post;
    $post = $post_data;
    setup_postdata($post);
    $before_section_image                           = get_field('before_section_image');
    $income_section_title                           = get_field('income_section_title');
    $income_section_title_quote                     = get_field('income_section_title_quote');
    $income_section_button_name                     = get_field('income_section_button_name');
    $income_section_textarea                        = get_field('income_section_textarea');
    $under_section_images                           = get_field('under_section_images');
?>

<section id="cover5-6" style="background:url('<?php echo $before_section_image['url']; ?>') center center;background-size:cover"></section>
<section id="cover5">
    <div class="section-content4 col-md-12">
        <div class="k45up"></div>
            <div class="container">
                <div class="row">
				    <div class="block-left col-md-offset-right-1 col-lg-4 col-md-10 col-sm-10">
				        <h2 class="up-title"><?php echo $income_section_title; ?></h2>
				        <h4><?php echo $income_section_title_quote; ?></h4>
				        <button id="click" class="btn btn-s btn-lg"><?php echo $income_section_button_name; ?></button>
				    </div>
				    <div class="block-right col-md-offset-1 col-lg-7 col-md-7 col-md-10 col-sm-12">
				   
                        <p class="lead lead-p"><?php echo $income_section_textarea; ?></p>
                        
                        
                        <?php echo do_shortcode( '[contact-form-7 id="41" title="Primary Contact Form"]' ); ?>
                        

                    </div>
                </div>
            </div>
        </div>
        <div class="k45down"></div>
</section>
        
<section id="cover5-6" style="background:url('<?php echo $under_section_images['url']; ?>') center center;background-size:cover"></section>



<?php get_footer(); ?>