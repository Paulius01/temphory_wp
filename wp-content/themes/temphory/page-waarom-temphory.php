<?php
/* Template Name: Waarom Temphory Page */

get_header(); ?>

<?php 
    $post_data = get_post( 82 );
    global $post;
    $post = $post_data;
    setup_postdata($post);
    $before_section_image                           = get_field('before_section_image');
    $income_section_title                           = get_field('income_section_title');
    $income_section_title_quote                     = get_field('income_section_title_quote');
    $income_section_mini_textarea                   = get_field('income_section_mini_textarea');
    $income_section_textarea_title                  = get_field('income_section_textarea_title');
    $income_section_textarea                        = get_field('income_section_textarea');
    $under_section_images                           = get_field('under_section_images');

    $income_section2_title                          = get_field('income_section2_title');
    $income_section2_title_quote                    = get_field('income_section2_title_quote');
    $income_section2_textarea_title                 = get_field('income_section2_textarea_title');
    $income_section2_textarea                       = get_field('income_section2_textarea');
    $under_section2_images                          = get_field('under_section2_images');
?>

    
    <section id="cover3-4" style="background:url('<?php echo $before_section_image['url']; ?>') center center;background-size:cover"></section>


    <section id="cover4">
		<div class="section-content3 col-md-12">
			<div class="k45up-orange"></div>
            <div class="container">
                <div class="row">
                    <div class="block-left col-lg-4 col-md-10 col-sm-10">
                        <h2 class="up-title"><?php echo $income_section_title; ?></h2>
                        <h4><?php echo $income_section_title_quote; ?></h4>
                        <p class="lead"><?php echo $income_section_mini_textarea; ?></p>
                    </div>
                    <div class="block-left col-lg-1"></div>
                    <div class="block-right col-lg-7 col-md-10 col-sm-10">
                        <h3 class="title3 up-title"><?php echo $income_section_textarea_title; ?></h3>
                        <p class="lead"><?php echo $income_section_textarea; ?></p>
                    </div>
                    <div class="k45"></div>
                </div>
			</div>
        </div>
    </section>


    <section id="cover4-5" style="background:url('<?php echo $under_section_images['url']; ?>') center center;background-size:cover"></section>

            
	<section id="cover5">
        <div class="section-content2 col-md-12">
			<div class="k45up"></div>
			<div class="container">
                <div class="row">
                    <div class="block-left col-lg-12 col-md-12 col-sm-10">
                        <h2><?php echo $income_section2_title; ?></h2>
                        <h4><?php echo $income_section2_title_quote; ?></h4>
                        <h3 class="title2 up-title"><?php echo $income_section2_textarea_title; ?></h3>
                        <p class="lead"><?php echo $income_section2_textarea; ?></p>
                    </div>
                    <div class="k45down"></div>
				</div>
			</div>
		</div>
	</section>
            
    <section id="cover5-6" style="background:url('<?php echo $under_section2_images['url']; ?>') center center;background-size:cover"></section


<?php get_footer(); ?>